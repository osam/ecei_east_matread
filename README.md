# ECEI_EAST_matread

GUI written in python3 and pyqt5.

GUI is designed to read .mat files of the EAST ECEI data.
The .mat files by default located in the "mat_files_input" folder

use as:
```
python3 ECEI_EAST_plot.py
```
